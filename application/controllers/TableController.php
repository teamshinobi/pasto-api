<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TableController extends KOALA_Controller 
{
	public function indexAction($restaurantId)
	{
		if ($this->input->method(true) === 'GET') {
			$this->load->model('tablemodel');
			
			$tables = $this->tablemodel->findAll($restaurantId);
			$this->outputToJson([ 'tables' => $tables ]);
		} else {
			$this->invalidHTTPMethod();
		}
	}
}
