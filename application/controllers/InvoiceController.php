<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceController extends KOALA_Controller
{
    public function newAction()
    {
	    if ($this->input->method(true) === 'POST') {

		    $form = $this->input->raw_input_stream;

		    $this->load->model('invoicemodel');

		    $this->invoicemodel->save(json_decode($form));

		    $this->outputToJson([ 'message' => 'success' ]);
	    } else {
		    $this->invalidHTTPMethod();
	    }
    }

    public function getActiveInvoiceAction($table)
    {
	    if ($this->input->method(true) === 'GET') {

	    	$this->load->model('invoicemodel');

		    $invoice = $this->invoicemodel->getActiveInvoice($table);

		    $this->outputToJson([ 'invoice' => $invoice ]);
	    } else {
		    $this->invalidHTTPMethod();
	    }
    }

    public function addOrderAction()
    {
	    if ($this->input->method(true) === 'POST') {

		    $form = $this->input->raw_input_stream;

		    $this->load->model('invoicemodel');

		    $this->invoicemodel->addOrders(json_decode($form));

		    $this->outputToJson([ 'message' => 'success' ]);
	    } else {
		    $this->invalidHTTPMethod();
	    }
    }

    public function cancelAction($invoiceId)
    {
	    if ($this->input->method(true) === 'POST') {
		    $this->load->model('invoicemodel');

		    $this->invoicemodel->cancelInvoice($invoiceId);

		    $this->outputToJson([ 'message' => 'success' ]);
	    } else {
		    $this->invalidHTTPMethod();
	    }
    }
}
