<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends KOALA_Controller 
{ 
	public function loginAction()
	{
		if ($this->input->method(true) === 'POST') {
			$this->load->model('usermodel');

			$form = $this->input->post(NULL, TRUE);

			$user = $this->usermodel->login($form);
			$this->outputToJson([ 'user' => $user ]);
		} else {
			$this->invalidHTTPMethod();
		}
    }
}
