<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FoodController extends KOALA_Controller
{
	public function indexAction($restaurantId)
	{
		if ($this->input->method(true) === 'GET') {
			$this->load->model('foodmodel');
			
			$foods = $this->foodmodel->findAll($restaurantId);

			$this->outputToJson([ 'foods' => $foods ]);
		} else {
			$this->invalidHTTPMethod();
		}
	}
}
