<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoicemodel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function save($form)
	{
		$this->db->trans_start();

		$invoice = $this->createInvoice($form);
		$this->db->insert('invoice', $invoice);
		$invoiceId =  $this->db->insert_id();

		$orders = $this->createOrders($form, $invoiceId);
		foreach ($orders as $order) {
			$this->db->insert('order_list', $order);
		}

		$this->db->trans_complete();
	}

	protected function createInvoice($form)
	{
		$invoice = $form->invoice;

		$date = new \DateTime('now');

		return [
			'user' => $invoice->user->id,
			'discount' => '1',
			'table_no' => $invoice->table->id,
			'date' => $date->format('Y-m-d H:i:s'),
			'total' => $invoice->total,
			'payment_change' => '0.0',
			'status' => $invoice->status,
			'payment_type' => $invoice->payment_type,
		];
	}

	public function addOrders($form)
	{
		$this->db->trans_start();

		foreach ($form->orders as $order) {
			$aOrder = [
				'invoice' => $form->invoice_id,
				'product' => $order->food->id,
				'quantity' => $order->quantity,
				'total' => $order->total,
				'status' => 1,
			];

			$this->db->insert('order_list', $aOrder);
		}

		$this->db->trans_complete();
	}

	public function cancelInvoice($invoiceId)
	{
		$this->db->trans_start();

		$this->db->delete('order_list', array('invoice' => $invoiceId));
		$this->db->delete('invoice', array('invoice_id' => $invoiceId));

		$this->db->trans_complete();
	}

	protected function createOrders($form, $invoiceId)
	{
		$jsonInvoice = $form->invoice;
		$orders = [];

		foreach ($jsonInvoice->orders as $order) {
			$orders[] = [
				'invoice' => $invoiceId,
				'product' => $order->food->id,
				'quantity' => $order->quantity,
				'total' => $order->total,
				'status' => 1,
			];
		}

		return $orders;
	}

	public function getActiveInvoice($table)
	{
		$this->db->select('i.invoice_id as id');
		$this->db->select('i.date as date');
		$this->db->select('i.total as total');
		$this->db->select('i.payment_change as payment_change');
		$this->db->select('i.status as status');
		$this->db->select('i.payment_type as payment_type');
		$this->db->from('invoice as i');
		$this->db->where([
			'table_no =' => $table,
			'status !=' => 'Paid',
		]);

/*		$this->db->where('i.status','Active');
		$this->db->or_where('i.status','Completed');*/

		$query = $this->db->get();

		$invoice = $query->row();
		$invoice->orders = $this->getOrders($invoice->id);

		foreach ($invoice->orders as $order) {
			$order->food = $this->getFood($order->food);
		}

		return $query->row();
	}

	protected function getOrders($invoiceId)
	{
		$this->db->select('o.order_id as id');
		$this->db->select('o.quantity as quantity');
		$this->db->select('o.product as food');
		$this->db->select('o.total as total');
		$this->db->from('order_list as o');
		$this->db->where([
			'invoice =' => $invoiceId,
		]);
		$query = $this->db->get();

		return $query->result();
	}

	protected function getFood($foodId) {
		$this->db->select('f.food_name as name');
		$this->db->select('f.price as price');
		$this->db->from('food_list as f');
		$this->db->where([
			'food_id =' => $foodId,
		]);

		$query = $this->db->get();

		return $query->row();
	}
}