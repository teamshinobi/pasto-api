<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tablemodel extends CI_Model
{
	const TABLE_TABLE = 'table_list';
	const INVOICE_TABLE = 'invoice';

	public function __construct()
	{
		parent::__construct();
	}

	public function findAll($restaurantId)
	{
		$this->db->select('t.table_id as id');
		$this->db->select('t.table_name as name');
		$this->db->from(self::TABLE_TABLE . ' as t');
		$this->db->where([
			'restaurant =' => $restaurantId,
		]);

		$query = $this->db->get();
		$result = $this->checkOrderStatus($query->result());

		return $result;
	}

	protected function checkOrderStatus($result)
	{
		foreach ($result as $table) {
			$this->db->select('i.invoice_id as id');
			$this->db->from(self::INVOICE_TABLE . ' as i');
			$this->db->where([
				'i.table_no' => $table->id,
				'i.status !=' => 'Paid',
			]);

			if (!empty($this->db->get()->result())) {
				$table->hasActiveOrder = true;
			} else {
				$table->hasActiveOrder = false;
			}
		}

		return $result;
	}
}
