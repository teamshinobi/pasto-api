<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Foodmodel extends CI_Model
{
	const FOOD_TABLE = 'food_list';
	const CATEGORY_TABLE = 'food_category';

	public function findAll($restaurantId)
	{
		$this->db->select('f.food_id as id');
		$this->db->select('f.food_name as name');
		$this->db->select('f.food_description as description');
		$this->db->select('f.price as price');
		$this->db->select('c.category_name as category');
		$this->db->select('i.quantity as inventory_quantity');

		$this->db->from(self::FOOD_TABLE . ' as f');
		$this->db->join(self::CATEGORY_TABLE . ' as c' , 'f.category_id = c.food_category_id');
		$this->db->join('inventory as i' , 'i.food = f.food_id', 'left');

		$this->db->where([
			'f.restaurant_id =' => $restaurantId,
		]);

		$query = $this->db->get();

		return $query->result_array();
		//return $this->groupByCategories($query->result_array());
	}

	protected function groupByCategories($foods)
	{	 
		$categories = array_values(array_unique(array_column($foods, 'category')));
		$groupedResult = [];

		foreach ($categories as $category) {
			$groupedResult[$category] = [];
		}

		foreach ($foods as $food) {
			$groupedResult[$food['category']][] = $food;
		}
	
		return $groupedResult;
	}
}