<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model
{
	const USER_TABLE = 'user_account';
	const ACCT_TYPE_TABLE = 'account_type';
	const EMPLOYEE_TABLE = 'employee';
	const RESTAURANT_TABLE = 'restaurant';

	public function __construct()
	{
		parent::__construct();
	}

	public function login($form)
	{
		$this->db->select('u.username');
		$this->db->select('u.user_id');
		$this->db->select('e.emp_first_name as first_name');
		$this->db->select('e.emp_last_name as last_name');
		$this->db->select('r.restaurant_name as restaurant');
		$this->db->select('r.restaurant_id as restaurant_id');
		$this->db->select('a.type_name as account');
			
		$this->db->from(self::USER_TABLE . ' as u');
		$this->db->join(self::EMPLOYEE_TABLE . ' as e' , 'u.emp_id = e.employee_id');
		$this->db->join(self::RESTAURANT_TABLE . ' as r', 'u.restaurant_id = r.restaurant_id');
		$this->db->join(self::ACCT_TYPE_TABLE . ' as a', 'u.user_type = a.account_id');

		$this->db->where([
			'username =' => $form['username'],
			'password =' => $form['password'], 
		]);

		$query = $this->db->get();

		if (empty($query->result())) {
			return null;
		}

		return $this->formatData($query->result()[0]);
	}

	protected function formatData($result)
	{
		return [
			'id' => $result->user_id,
			'username' => $result->username,
			'first_name' => $result->first_name,
			'last_name' => $result->last_name,
			'restaurant' => [
				'id' => $result->restaurant_id,
				'name' => $result->restaurant,
			],
			'account' => $result->account,
		];
	}
}
