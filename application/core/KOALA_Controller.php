<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class KOALA_Controller extends CI_Controller 
{
	public function __construct()
	{
			parent::__construct();
	}

	protected function invalidHTTPMethod()
	{
		$this->outputToJson([
			'message' => 'Unable to find URL.',
		], 400);
	}

	protected function outputToJson($response, $status = 200)
	{
		$this->output
			->set_status_header($status)
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}
}